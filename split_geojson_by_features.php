!# /usr/bin/env php
<?php 

$geo = file_get_contents("./municipios/export (3).json");
$geo = json_decode($geo);
foreach(
    array_map(
        fn($f) => [
            "type" => "FeatureCollection", 
            "features" => [$f]
        ], 
        $geo->features
    )
    AS $k => $v
) file_put_contents($k . " - " . ($v["features"][0]->properties->name ?? "xxx") . ".json", json_encode($v));
    

